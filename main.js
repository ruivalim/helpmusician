const {app, BrowserWindow} = require('electron');

let mainWindow;

function createWindow () {
	mainWindow = new BrowserWindow({
		icon: 'icons/helpmusician.png',
		show: false
	});
	mainWindow.maximize();
	mainWindow.loadFile('index.html');
	mainWindow.show();
	mainWindow.on('closed', function () {
		mainWindow = null;
	});
}

app.on('ready', createWindow)
 
app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function () {
	if (mainWindow === null) {
		createWindow();
	}
});

