function scroll_(time){
	$("html, body").scrollTop(0);
	$('html, body').stop();
	if( time != null ){
		if( time != 0 ){
			window.setTimeout(function(){	
				let h = $("#Execucao").height();
				$('html, body').animate({
			        scrollTop: h
			    }, 1000 * time);
			}, 100);
		}
	}
	window.onscroll = function(ev) {
	    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
			$('html, body').stop();
	    }
	};
}



let colunas_count = sessionStorage.getItem("colunas_count");
let font_size = sessionStorage.getItem("font_size");

if( colunas_count !== null ){
	document.getElementById("Letra_atual").classList.add(colunas_count);
}
if( font_size !== null ){
	document.getElementById("Letra_atual").classList.add(font_size);
}

document.getElementById("Letra_atual").addEventListener("click", function(){


	let atual = sessionStorage.getItem("atual");
	atual = parseInt(atual);

	atual = atual+1;
	let proxima = atual+1;

	let data = sessionStorage.getItem("playlist");
	data = JSON.parse(data);

	if( typeof data[atual] != "undefined" ){
		let scroll = sessionStorage.getItem("scroll");
		if( scroll !== null ){
			scroll_(scroll);
		}
		sessionStorage.setItem("atual", atual);

		let atual_musica = data[atual];

		let time = sessionStorage.getItem("scroll");

		if( typeof data[proxima] != "undefined" ){
			let proxima_musica = data[proxima];
			document.getElementById("Proxima_musica").innerHTML = `Próxima: ${proxima_musica.musica}`;
		}else{
			document.getElementById("Proxima_musica").innerHTML = "";
		}

		document.getElementById("Musica_atual").innerHTML = `Atual: ${atual_musica.musica}`;
		document.getElementById("Letra_atual").innerHTML = `${atual_musica.letra}`;

		if ( time !== null || time !== 0) {
			scroll_(time);
		}
	}else{
		alert("A Playlist acabou!");
	}
});

document.getElementById("Iniciar_programa").addEventListener("click", function(){
	let data = sessionStorage.getItem("playlist");
	

	let scroll = sessionStorage.getItem("scroll");
	if( scroll !== null ){
		scroll_(scroll);
	}

	if( data != null ){
		data = JSON.parse(data);

		if( typeof data[0] !== "undefined" ){
			sessionStorage.setItem("atual", 0);

			let primeira_musica = data[0];

			let time = sessionStorage.getItem("scroll");

			if( typeof data[1] != "undefined" ){
				let proxima_musica = data[1];
				document.getElementById("Proxima_musica").innerHTML = `Próxima: ${proxima_musica.musica}`;
			}

			document.getElementById("Musica_atual").innerHTML = `Atual: ${primeira_musica.musica}`;
			document.getElementById("Letra_atual").innerHTML = `${primeira_musica.letra}`;

			if ( time !== null || time !== 0) {
				scroll_(time);
			}
		}else{
			alert("É necessario criar uma playlist!");
		}
	}else{
		alert("É necessario criar uma playlist!");
	}
});