"use strict";

let user = localStorage.getItem("user");

if( user == null ){
	alert("Configurando Aplicativo!");
	var request = new XMLHttpRequest();
	request.open('GET', 'http://www.geoplugin.net/json.gp?jsoncallback=?', true);

	request.onload = function() {
		if (this.status >= 200 && this.status < 400) {
			var data = this.response.replace(/%3F/g, "").replace("(", "").replace(")", "");
			data = JSON.parse(data);

			user = {
				_id: token(),
				today: new Date(),
				user_data: data
			};

			alert("Aplicativo Configurado! \n ID:"+user._id);

			user = JSON.stringify(user);

			localStorage.setItem("user", user);
		} else {
			console.log(this);
		}
	};

	request.onerror = function() {
		console.log("Linha 17");
	};

	request.send();
}

user = localStorage.getItem("user");
user = JSON.parse(user);

user_online(user);

let musicas = localStorage.getItem("musicas");

if( musicas != null ){
	musicas = JSON.parse(musicas);

	musicas.map((item) => {
		add_on_list(item);
	});
}

let repertorios = localStorage.getItem("repertorios");

if( repertorios != null ){
	repertorios = JSON.parse(repertorios);

	repertorios.map((item) => {
		add_repertorio(item);
	});
}

let playlist = sessionStorage.getItem("playlist");

if( playlist != null ){
	playlist = JSON.parse(playlist);

	playlist.map((item) => {
		add_playlist(item, false);
	});
}

function rand() {
    return Math.random().toString(36).substr(2);
};

function token() {
    return rand() + rand();
};

function user_online(data){
	let data_sent = JSON.stringify(data);
}

function add_on_list(musica){
	let item = document.createElement("tr");
		item.setAttribute("class", "pesquisa_item");

	let a1 = document.createElement("a");
		a1.setAttribute('class', 'show_letra');
		a1.setAttribute("id_letra", musica._id);
    let t1 = document.createTextNode(musica.artista+" - "+musica.musica);
    	a1.appendChild(t1);

	let a2 = document.createElement("a");
		a2.setAttribute('class', 'add_playlist');
		a2.setAttribute("id_letra", musica._id);
    let t2 = document.createTextNode("Adicionar a Playlist");
    	a2.appendChild(t2);

	let a3 = document.createElement("a");
		a3.setAttribute('class', 'excluir_musica');
		a3.setAttribute("id_letra", musica._id);
    let t3 = document.createTextNode("Excluir");
    	a3.appendChild(t3);

    let td1 = document.createElement("td");
    let td2 = document.createElement("td");
    let td3 = document.createElement("td");

    td1.appendChild(a1);
    td2.appendChild(a2);
    td3.appendChild(a3);

    item.appendChild(td1);
    item.appendChild(td2);
    item.appendChild(td3);

	document.getElementById("MusicasSalva").appendChild(item);

	a1.addEventListener("click", function(){
		let id = a1.getAttribute("id_letra");

		show_Letra(id);
	});
	a2.addEventListener("click", function(){
		let id = a2.getAttribute("id_letra");

		let musicas = localStorage.getItem("musicas");

		musicas = JSON.parse(musicas);

		musicas.map((item) => {
			if( item._id == id ){
				add_playlist(item);
			}
		});
	});
	a3.addEventListener("click", function(){
		let id = a3.getAttribute("id_letra");

		let musicas = localStorage.getItem("musicas");

		musicas = JSON.parse(musicas);

		musicas.map((item) => {
			if( item._id == id ){
			    const index = musicas.indexOf(item);

			    if (index !== -1) {
			        musicas.splice(index, 1);
			    }
			}
		});
		item.remove();
		musicas = JSON.stringify(musicas);

		localStorage.setItem("musicas", musicas);
	});
}

function show_Letra(id){
	let musicas = localStorage.getItem("musicas");

	musicas = JSON.parse(musicas);

	musicas.map((item) => {
		if( item._id == id ){
			localStorage.setItem("LETRAPARAVER", item.letra);

			const electron = require('electron')
			const path = require('path')
			const BrowserWindow = electron.remote.BrowserWindow
			const modalPath = path.join('file://', __dirname, 'letra.html')

			let win = new BrowserWindow({ width: 400, height: 400 })
			win.on('close', function () { win = null })
			win.loadURL(modalPath)
			win.show()
		}
	});
}

function add_playlist(musica, atualizar = true){
	let item = document.createElement("tr");

	let a1 = document.createElement("a");
		a1.setAttribute('class', 'show_letra');
		a1.setAttribute("id_letra", musica._id);
    let t1 = document.createTextNode(musica.artista+" - "+musica.musica);
    	a1.appendChild(t1);

	let a2 = document.createElement("a");
		a2.setAttribute('class', 'remove_playlist');
		a2.setAttribute("id_letra", musica._id);
    let t2 = document.createTextNode("Remover");
    	a2.appendChild(t2);

    let td1 = document.createElement("td");
    let td2 = document.createElement("td");

    td1.appendChild(a1);
    td2.appendChild(a2);

    item.appendChild(td1);
    item.appendChild(td2);

	document.getElementById("Playlist").appendChild(item);

	a1.addEventListener("click", function(){
		let id = a1.getAttribute("id_letra");
		
		show_Letra(id);
	});
    a2.addEventListener("click", function(){
		let id = a2.getAttribute("id_letra");

		remove_da_playlist(id);
		item.remove();
	});

    if( atualizar == true ){
		let playlist = sessionStorage.getItem("playlist");
		if( playlist == null ){
			playlist = [];
		}else{
			playlist = JSON.parse(playlist);
		}

		playlist.push(musica);

		playlist = JSON.stringify(playlist);

		sessionStorage.setItem("playlist", playlist);
    }
}

function remove_da_playlist(id){
	let playlist = sessionStorage.getItem("playlist");
	playlist = JSON.parse(playlist);

	playlist.map((item) => {
		if( item._id == id ){
		    const index = playlist.indexOf(item);

		    if (index !== -1) {
		        playlist.splice(index, 1);
		    }
		}
	});

	playlist = JSON.stringify(playlist);

	sessionStorage.setItem("playlist", playlist);
}

function remove_item_from_array(array, element) {
    const index = array.indexOf(element);

    if (index !== -1) {
        array.splice(index, 1);
    }
}

function remove_repertorio(name){
	let repertorios = localStorage.getItem("repertorios");
	repertorios = JSON.parse(repertorios);

    const index = repertorios.indexOf(name);

    if (index !== -1) {
        repertorios.splice(index, 1);
    }

	repertorios = JSON.stringify(repertorios);

	localStorage.setItem("repertorios", repertorios);
	localStorage.removeItem("r_"+name);
}

function add_repertorio(r_name){
	let item = document.createElement("tr");

	let a1 = document.createElement("a");
    let t1 = document.createTextNode(r_name);
    	a1.appendChild(t1);

	let a2 = document.createElement("a");
    let t2 = document.createTextNode("Remover");
    	a2.appendChild(t2);

    let td1 = document.createElement("td");
    let td2 = document.createElement("td");

    td1.appendChild(a1);
    td2.appendChild(a2);

    item.appendChild(td1);
    item.appendChild(td2);

	document.getElementById("Repertorios").appendChild(item);

	a1.addEventListener('click', function(){
		let data = localStorage.getItem("r_"+r_name);
		data = JSON.parse(data);

		document.getElementById("Playlist").innerHTML = "";
		sessionStorage.removeItem("playlist");

		data.map((item) => {
			add_playlist(item);
		});
	});

	a2.addEventListener("click", function(){
		remove_repertorio(r_name);
		item.remove();
	})
}

document.getElementById("Save_song").addEventListener("click", function(event){
	event.preventDefault();

	let artista = document.getElementById("Artista_save").value;
	let song = document.getElementById("Song_save").value;

	document.getElementById("Artista_save").value = "";
	document.getElementById("Song_save").value = "";
	document.getElementById("Artista_save").focus();

	let vagalume_api = "https://api.vagalume.com.br/search.php?apikey=7d77f0b0f43acad356a54b1301f7e7c6&art="+artista+"&mus="+song;


	var request = new XMLHttpRequest();
	request.open('GET', vagalume_api, true);

	request.onload = function() {
		if (this.status >= 200 && this.status < 400) {
			var data = JSON.parse(this.response);
			if( data['type'] == "song_notfound" || data['type'] == "notfound" ){
	    		alert("Música não encontrada, por favor verifique os dados inseridos e tente novamente!");
	    	}else{
		    	let letra = data.mus[0].text;

		    	letra = letra.replace(/(?:\r\n|\r|\n)/g, '<br>');

		    	let musica = {
		    		_id: token(),
		    		tipo: "musica",
		    		artista: artista,
		    		musica: song,
		    		letra: letra
		    	};

		    	let musicas = localStorage.getItem("musicas");

		    	if( musicas == null ){
		    		musicas = [];
		    	}else{
		    		musicas = JSON.parse(musicas);
		    	}

		    	musicas.push(musica);

		    	let save = JSON.stringify(musicas);

		    	localStorage.setItem("musicas", save);

		    	add_on_list(musica);
	    	}
		} else {
			console.log(this);
		}
	};

	request.onerror = function(xhr, ajaxOptions, thrownError) {
		console.log(xhr, ajaxOptions, thrownError);
	};

	request.send();
});

document.getElementById("AutoSCROLL_VALUE").addEventListener("change", function(){
	let val = document.getElementById("AutoSCROLL_VALUE").value;
	sessionStorage.setItem("scroll", val);
	console.log(val);
});

document.getElementById("QUANTIDADE_DE_COLUNAS").addEventListener("change", function(){
	let val = document.getElementById("QUANTIDADE_DE_COLUNAS").value;
	sessionStorage.setItem("colunas_count", val);
	console.log(val);
});

document.getElementById("TAMANHO_DA_FONTE").addEventListener("change", function(){
	let val = document.getElementById("TAMANHO_DA_FONTE").value;
	sessionStorage.setItem("font_size", val);
	console.log(val);
});

document.getElementById("LIMPARAPP").addEventListener("click", function(){
	localStorage.clear();
	sessionStorage.clear();
	const {app} = require('electron').remote;
	app.relaunch();
	app.exit();
});

document.getElementById("FullScreenButton").addEventListener("click", function(){
	let app = require('electron');
	var windowBrowser = app.remote.getCurrentWindow();

	let fs = sessionStorage.getItem("fs");

	console.log(fs)

	if( fs == null || fs == "false" ){
		windowBrowser.setFullScreen(true);	
		sessionStorage.setItem("fs", "true");
		$("#FullScreenButton").html("Fechar Fullscreen");
	}

	if( fs == "true" ){
		windowBrowser.setFullScreen(false);
		sessionStorage.setItem("fs", "false");
		$("#FullScreenButton").html("Abrir Fullscreen");
	}

	app = null;
	windowBrowser = null;
});

document.getElementById("BACKUPAPP").addEventListener("click", function(){
	var app = require('electron').remote; 
	var dialog = app.dialog;
	var fs = require('fs');

	let data = localStorage.getItem("musicas");

	if( data != null ){
		let content = data;

		dialog.showSaveDialog((fileName) => {
		    if (fileName === undefined){
		        console.log("You didn't save the file");
		        return;
		    }

		    fs.writeFile(fileName, content, (err) => {
		        if(err){
		            alert("Algum erro ocorreu: "+ err.message)
		        }
		                    
		        alert("O arquivo de backup foi salvo!");
		    });
		}); 
	}
});

document.getElementById("DOWNLOADPLAYLIST").addEventListener("click", function(){
	var app = require('electron').remote; 
	var dialog = app.dialog;
	var fs = require('fs');


	let data = sessionStorage.getItem("playlist");
	
	if( data != null ){
		data = JSON.parse(data);

		let content;

		data.map((item) => {
			console.log(item);
			content += item.musica;
			content += "\n";
			content += item.letra.replace(/<br>/g, '\n');
			content += "\n";
			content += "\n";
		});

		content = content.replace(/undefined/g, '');

		dialog.showSaveDialog((fileName) => {
		    if (fileName === undefined){
		        console.log("You didn't save the file");
		        return;
		    }

		    fs.writeFile(fileName, content, (err) => {
		        if(err){
		            alert("Algum erro ocorreu: "+ err.message)
		        }
		                    
		        alert("O arquivo foi salvo");
		    });
		}); 
	}
	app = null;
	dialog = null;
	fs = null;
});

document.getElementById("SALVARREPERTORIO").addEventListener("click", function(){
	const prompt = require('electron-prompt');

	prompt({
	    title: 'Adicionar Repertório',
	    label: 'Qual o nome do repertório?',
	    type: 'input'
	}).then((r) => {
		let r_name = r;

		if( r_name !== null ){
			let playlist = sessionStorage.getItem("playlist");
			let repertorios = localStorage.getItem("repertorios");

			if( repertorios == null ){
				repertorios = [];
			}else{
				repertorios = JSON.parse(repertorios);
			}

			repertorios.push(r_name);

			repertorios = JSON.stringify(repertorios);

			localStorage.setItem("repertorios", repertorios);
			localStorage.setItem("r_"+r_name, playlist);


			let item = document.createElement("tr");

			let a1 = document.createElement("a");
		    let t1 = document.createTextNode(r_name);
		    	a1.appendChild(t1);

			let a2 = document.createElement("a");
		    let t2 = document.createTextNode("Remover");
		    	a2.appendChild(t2);

		    let td1 = document.createElement("td");
		    let td2 = document.createElement("td");

		    td1.appendChild(a1);
		    td2.appendChild(a2);

		    item.appendChild(td1);
		    item.appendChild(td2);

			document.getElementById("Repertorios").appendChild(item);

			alert("Repertório salvo com sucesso");

			a1.addEventListener('click', function(){
				let data = localStorage.getItem("r_"+r_name);
				data = JSON.parse(data);
				
				document.getElementById("Playlist").innerHTML = "";
				sessionStorage.removeItem("playlist");
				
				data.map((item) => {
					add_playlist(item);
				});
			});

			a2.addEventListener("click", function(){
				remove_repertorio(r_name);
				item.remove();
			});
		}
	});
});

document.getElementById("ZERARPLAYLIST").addEventListener("click", function(){
	document.getElementById("Playlist").innerHTML = "";
	sessionStorage.removeItem("playlist");
});